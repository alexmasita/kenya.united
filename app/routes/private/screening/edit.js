import Route from '@ember/routing/route';

export default class PrivateScreeningEditRoute extends Route {
    model(params) {
        console.log('params.screening_id');
        console.log(params.screening_id);
        return this.store.findRecord('screening', params.screening_id);
    }
}
