import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
export default class PrivateAddPhoneRoute extends Route {
    @service globalCheck
    activate() {
        this.globalCheck.hasPhoneRedirect();
    }
}
