import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
export default class ApplicationRoute extends Route.extend(ApplicationRouteMixin, {
    globalCheck: service(),
    currentUser: service(),
    session: service(),
    beforeModel() {
        return this._loadCurrentUser();
    },
    sessionAuthenticated() {
        console.log('sessionAuthenticated called');
        this._loadCurrentUser();
        this._super(...arguments);
        console.log('super finally called');
    },
    async _loadCurrentUser() {
        try {
            console.log('_loadCurrentUser called');
            await this.currentUser.load();
            this.globalCheck.addPhone();
        } catch (err) {
            console.log('application error called with err');
            console.log(err);
            await this.session.invalidate();
        }
    }
}) {
    @service session;
    @service router;
    @service globalCheck;
    constructor() {
        super(...arguments);
        this.router.on('routeWillChange', (transition) => {
            console.log('willyTransition called');
            console.log('session');
            console.log(this.session);
            console.log(this.session.isAuthenticated);
            this.globalCheck.addPhone()
        })
    }
    // beforeModel() {
    //     return this._loadCurrentUser();
    // }
    // async sessionAuthenticated() {
    //     console.log('Application session Authenticated called');
    //     try {
    //         //let _super = this._super;
    //         await this._loadCurrentUser();
    //         //_super.call(this, ...arguments);
    //         this._super(...arguments);
    //     } catch (error) {
    //         console.log('sessionAuthenticated error');
    //         console.log(error);
    //     }

    // }

}