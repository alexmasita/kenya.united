import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class LoginController extends Controller {
    queryParams = ['email'];
    @tracked email = null;
}
