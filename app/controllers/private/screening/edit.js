import Controller from '@ember/controller';
import { alias } from '@ember/object/computed';
import { action } from '@ember/object';
export default class PrivateScreeningEditController extends Controller {
    @alias('model') editQuestion;
    @action
    saveQuestion({ title, post }) {
        this.editQuestion.title = title;
        this.editQuestion.post = post;
        this.editQuestion.save().then(() => {
            this.transitionToRoute('private.screening.index');
        });
    }
}
