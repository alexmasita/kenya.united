import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { alias } from '@ember/object/computed';

export default class PrivateScreeningAddController extends Controller {
    @service store;
    @alias('model') newQuestion;
    constructor() {
        super(...arguments);
        this.router.on('routeWillChange', (transition) => {
            console.log('route will change entered');
            if (this.newQuestion.hasDirtyAttributes) {
                this.newQuestion.rollbackAttributes();
                console.log('route will change has rolledbackattributes');
            }
        });
    }
    @action
    saveQuestion({ title, post }) {
        console.log('saveQuestion called');
        let newQuestion = this.newQuestion;
        newQuestion.title = title;
        newQuestion.post = post;
        //const newQuestion = this.store.createRecord('screening', { title, post });
        newQuestion.save().then(() => {
            this.transitionToRoute('private.screening.index');
        });
    }
}
