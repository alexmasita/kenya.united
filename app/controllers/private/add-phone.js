import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PrivateAddPhoneController extends Controller {
    queryParams = ['hasCodeToVerify'];
    @tracked hasCodeToVerify = false;
    @action
    setCodeToVerify(status) {
        this.hasCodeToVerify = status;
    }
}
