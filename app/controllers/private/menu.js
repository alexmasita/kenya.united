import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class PrivateMenuController extends Controller {
    @service session
    @service currentUser
    @action
    signOut() {
        this.session.invalidate();
    }
}
