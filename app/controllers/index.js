import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
export default class IndexController extends Controller {
    @service session
    @action
    signOut() {
        console.log('sign out called');
        this.session.invalidate();
    }
    prevent(e) {
        e.preventDefault();
    }

}
