import Service from '@ember/service';

export default class WindowService extends Service {
    get windowRef() {
        return window;
    }
}
