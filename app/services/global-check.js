import Service, { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
export default class GlobalCheckService extends Service {
    @service session;
    @service router;
    @service currentUser;
    addPhone() {
        if (this.session.isAuthenticated) {
            if (!isPresent(this.session.data.authenticated.user.phoneNumber)) {
                console.log('this.currentUser.phoneNumber isNot present');
                this.router.transitionTo('private.addPhone');
            }
        }
    }
    hasPhoneRedirect() {
        if (this.session.isAuthenticated && isPresent(this.session.data.authenticated.user.phoneNumber)) {
            this.currentUser.load();
            this.router.transitionTo('index');
        }
    }
}
