import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class CurrentUserService extends Service {
    @service session;
    @service store;
    @tracked displayName;
    @tracked phoneNumber;
    async load() {
        //let user = this.session.data.authenticated.user;
        // if (userId) {
        //     let user = await this.store.findRecord('user', userId);
        //     this.set('user', user);
        // }
        if (this.session.data && this.session.data.authenticated && this.session.data.authenticated.user && this.session.data.authenticated.user.uid) {
            this.displayName = this.session.data.authenticated.user.displayName;
            this.phoneNumber = this.session.data.authenticated.user.phoneNumber;
        }
    }
}
