import Modifier from 'ember-modifier';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class AutoSignOutModifier extends Modifier {
    @service session;
    @action onClick(e) {
        e.preventDefault();
        this.session.invalidate();
    }
    didInstall() {
        this.element.addEventListener('click', this.onClick, true);
    }
    willRemove() {
        this.element.removeEventListener('click', this.onClick, true);
    }
}
