import Model, { attr } from '@ember-data/model';

export default class ScreeningModel extends Model {
    @attr('string') title;
    @attr('string') post;
    @attr('number') yesScreeningId;
    @attr('number') noScreeningId;
}
