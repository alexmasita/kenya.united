import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { bind } from '@ember/runloop';
import firebase from 'firebase';
import { assign } from '@ember/polyfills';
export default class SignInPhoneComponent extends Component {
    @service session
    @service currentUser
    @service window
    @service globalCheck
    @action
    signOut() {
        this.session.invalidate();
    }
    @action
    handlePhoneUpdate(phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    @action
    didInsert() {
        this.window = this.window.windowRef;
        console.log('this.window');
        console.log(this.window);
        let parameters = assign({ 'size': 'invisible' }, {
            'callback': bind(this, this.successCallback),//get(that, 'successCallback').bind(that),
            'expired-callback': bind(this, this.expiredCallback)  //get(that, 'expiredCallback').bind(that),
        });
        console.log('parameters');
        console.log(parameters);

        //firebase.initializeApp(ENV.firebase);
        //let firebase = this.firebase.create(this);
        console.log('firebase');
        console.log(firebase);

        this.window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('sign-in-button', parameters);
        console.log('this.window.recaptchaVerifier');
        console.log(this.window.recaptchaVerifier);

    }
    submit(e) {
        e.preventDefault();
    }
    successCallback() {
        console.log('success callback called');
    }
    expiredCallback() {
        console.log('expired callback called');
    }

    @action
    signIn() {
        console.log('signIn called');
        let appVerifier = this.window.recaptchaVerifier;
        console.log('appVerifier');
        console.log(appVerifier);
        //firebase.auth().signInWithPhoneNumber(this.phoneNumber, appVerifier)
        //this.session.data.authenticated.user.linkWithPhoneNumber(this.phoneNumber, appVerifier)
        //this.session.data.authenticated.user.linkWithPhoneNumber(this.phoneNumber, appVerifier)
        firebase.auth().currentUser.linkWithPhoneNumber(this.phoneNumber, appVerifier)
            .then((confirmationResult) => {
                // SMS sent. Prompt user to type the code from the message, then sign the
                // user in with confirmationResult.confirm(code).
                console.log('confirmationResult');
                console.log(confirmationResult);
                this.window.confirmationResult = confirmationResult;
                this.args.setCodeToVerify(true);

            }).catch(function (error) {
                // Error; SMS not sent
                console.log('error');
                console.log(error);
            });


    }
    @action
    verifyCode() {
        this.window.confirmationResult
            .confirm(this.code)
            .then(result => {

                this.user = result.user;
                console.log('success phone user object');
                console.log(this.user);
                console.log('this.session.data.authenticated.user');
                console.log(this.session.data.authenticated.user);
                this.globalCheck.hasPhoneRedirect();
            })
            .catch(error => console.log(error, "Incorrect code entered?"));
    }
}
