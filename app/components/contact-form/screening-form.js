import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ContactFormScreeningFormComponent extends Component {
    get title() {
        return this._title || this.args.question.title;
    }
    set title(value) {
        this._title = value;
    }
    get post() {
        return this._post || this.args.question.post;
    }
    set post(value) {
        this._post = value;
    }
    @action
    saveQuestion() {
        console.log('save question from component');
        this.args.saveQuestion({ title: this.title, post: this.post });
    }
    prevent(e) {
        e.preventDefault();
    }
}
