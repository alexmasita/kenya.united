import Component from '@glimmer/component';
import { action } from '@ember/object';
export default class OwlCarouselSingleSliderComponent extends Component {
    @action
    didInsert() {
        console.log('didInsert called');
        console.log(this);
        // later(this, function () {
        //     this.singleSlider = $('.single-slider');
        //     this.singleSlider.trigger('destroy.owl.carousel');
        //     this.singleSlider.owlCarousel({ loop: false, margin: 15, nav: false, lazyLoad: true, items: 1, autoplay: false, autoplayTimeout: 4000 });

        // }, 30);
        this.fullPage();
    }
    // @action
    // destroy() {
    //     console.log('destroy called');
    //     //this.singleSlider.owlCarousel('destroy');
    //     this.singleSlider.trigger('destroy.owl.carousel');
    // }
    // @action
    // onResize() {
    //     this.fullPage();
    // }
    //Caption Images
    fullPage() {
        $('.caption').each(function () {
            var captionHeight = $(this).data('height');
            $(this).css('height', captionHeight)
        })
    }
}
