import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class OwlCarouselWalletCaptionComponent extends Component {
    @action
    didInsert() {
        console.log('didInsert called');
        console.log(this);
        this.fullPage();
    }
    //Caption Images
    fullPage() {
        $('.caption').each(function () {
            var captionHeight = $(this).data('height');
            $(this).css('height', captionHeight)
        })
    }
}
