import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class OwlCarouselScreeningCaptionComponent extends Component {
    @action
    didInsert() {
        console.log('didInsert called');
        console.log(this);
        this.fullPage();
    }
    //Caption Images
    fullPage() {

        $('.caption').each(function () {
            var windowHeight = $(window).height();
            var captionHeight = $(this).data('height');
            if (captionHeight === "cover-header") {
                $(this).css('height', windowHeight)
                if (!header.length) {
                    pageContent.css('padding-bottom', '0px');
                    $(this).find('.caption-center, .caption-bottom, .caption-top').css('margin-top', '0px');
                }
                if (header.length) {
                    $(this).find('.caption-center, .caption-bottom, .caption-top').css('margin-top', '25px')
                }

            }
            $(this).css('height', captionHeight)
        })
    }
}
