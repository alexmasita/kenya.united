import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class SignUpComponent extends Component {
    @service firebase;
    @service session;
    @service router;
    @action
    signUp() {
        console.log('signUp called');
        let email = this.email;
        let password = this.password;
        this.firebase.auth().createUserWithEmailAndPassword(email, password).then(({ user }) => {
            this.session.authenticate('authenticator:firebase', (auth) => {
                return auth.signInWithEmailAndPassword(email, password);
            });
        }).catch((error) => {

            var errorCode = error.code;
            var errorMessage = error.message;
            console.log('errorCode');
            console.log(errorCode);
            console.log('errorMessage');
            console.log(errorMessage);
            if (errorCode === 'auth/email-already-in-use') {
                console.log('email:', email);
                this.router.transitionTo('login', {
                    queryParams: { email }
                });

            }
        });
    }

}
