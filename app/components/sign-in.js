import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class SignInComponent extends Component {
    @service session
    @service router
    get email() {
        return this._email || this.args.email;
    }
    set email(x) {
        this._email = x;
    }
    @action
    signIn() {
        let email = this.email;
        console.log('email:', email);
        this.session.authenticate('authenticator:firebase', (auth) => {
            return auth.signInWithEmailAndPassword(this.email, this.password).catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log('errorCode');
                console.log(errorCode);
                console.log('errorMessage');
                console.log(errorMessage);
                if (errorCode == 'auth/user-not-found') {
                    this.router.transitionTo('register');
                }
            });
        });
    }
}
