import EmberRouter from '@ember/routing/router';
import config from './config/environment';
import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';
export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('menu');
  this.route('register');
  this.route('private', function () {
    this.route('menu');
    this.route('profile', function () {
      this.route('edit');
      this.route('image');
    });
    this.route('donate');
    this.route('wallet', function () { });
    this.route('screen');
    this.route('screening', function () {
      this.route('add');
      this.route('edit', { path: 'edit/:screening_id' });
    });
    this.route('addPhone');
  });
  this.route('login');
  this.route('errorEmailInUse');
  this.route('contact');
  this.route('login-phone');
});
