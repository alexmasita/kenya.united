import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Controller | private/screening/edit', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let controller = this.owner.lookup('controller:private/screening/edit');
    assert.ok(controller);
  });
});
