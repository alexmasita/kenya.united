import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | private/addPhone', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:private/add-phone');
    assert.ok(route);
  });
});
