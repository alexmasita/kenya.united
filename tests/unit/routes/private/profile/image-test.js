import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | private/profile/image', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:private/profile/image');
    assert.ok(route);
  });
});
