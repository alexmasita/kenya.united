import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | private/donate', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    let route = this.owner.lookup('route:private/donate');
    assert.ok(route);
  });
});
