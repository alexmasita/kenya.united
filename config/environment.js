'use strict';

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'kenya-united',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },
    phoneInput: {
      lazyLoad: false
    },
    firebase: {
      apiKey: 'AIzaSyA48Z__8uf71WPSHRVuEU0fdFiYKGYINSw',
      authDomain: 'kenya-united.firebaseapp.com',
      databaseURL: 'https://kenya-united.firebaseio.com',
      projectId: 'kenya-united',
      storageBucket: 'kenya-united.appspot.com',
      messagingSenderId: '330361504545',
      appId: '1:330361504545:web:97b2f3caecec89bf33b460',
      measurementId: 'G-84D5T5M8WF'
    },
    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
  }

  return ENV;
};
